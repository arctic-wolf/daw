fn main() {
    println!(
        "{} - Arctic Fox Daemon {}",
        env!("CARGO_BIN_NAME"),
        env!("CARGO_PKG_VERSION")
    );
}
